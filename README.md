# KSS Beacon

Node.js based bluetooth endpoint kiosk for laptops and mini computers. Runs on Debian-based OS.
Specifically targets Raspberry Pi Zero W running Raspbian.
